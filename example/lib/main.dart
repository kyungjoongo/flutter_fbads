import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_fbads/flutter_fbads.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
    @override
    _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
    String _platformVersion = 'Unknown';

    @override
    void initState() {
        super.initState();
        initPlatformState();
    }

    // Platform messages are asynchronous, so we initialize in an async method.
    Future<void> initPlatformState() async {
        String platformVersion;
        try {
            platformVersion = await FlutterFbads.platformVersion;
        } on PlatformException {
            platformVersion = 'Failed to get platform version.';
        }

        if (!mounted) return;

        setState(() {
            _platformVersion = platformVersion;
        });
    }

    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            home: Scaffold(
                appBar: AppBar(
                    title: const Text('Plugin example app'),
                ),
                body: Center(
                    child: Column(
                        children: <Widget>[
                            RaisedButton(
                                onPressed: () async {
                                    FlutterFbads.showInitAd('577644512710121_577644722710100');
                                },
                                child: Text('showInitAd'),
                            ),
                            RaisedButton(
                                onPressed: () async {
                                    var result = await FlutterFbads.platformVersion;
                                    print('result===>$result');
                                },
                                child: Text('result'),
                            ),
                            RaisedButton(
                                onPressed: () {
                                    print('sldkflsdkflksd');
                                },
                                child: Text('dfksdlfkdslkf'),
                            ),
                        ],
                    )
                ),
            ),
        );
    }
}
