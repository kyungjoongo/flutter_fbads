package com.example.extractprojectexample;

import android.annotation.SuppressLint;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;

public class MainActivity extends AppCompatActivity {


    String youtubeLink = "https://www.youtube.com/watch?v=ShZ978fBl6Y";

    @SuppressLint("StaticFieldLeak")
    public void sendMessage(View view) {
        new YouTubeExtractor(this) {
            @Override
            public void onExtractionComplete(SparseArray<YtFile> ytFiles, VideoMeta vMeta) {
                if (ytFiles != null) {
                    int itag = 247;//137
                    String downloadUrl = ytFiles.get(itag).getUrl();
                    Log.d("SDLFKD", downloadUrl);

                }
            }
        }.extract(youtubeLink, true, true);
    }


    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new YouTubeExtractor(this) {
            @Override
            public void onExtractionComplete(SparseArray<YtFile> ytFiles, VideoMeta vMeta) {
                if (ytFiles != null) {

                    //mediaLink = ytFiles.get(22) != null ? ytFiles.get(22).getUrl() : (ytFiles.get(18) != null) ? ytFiles.get(18).getUrl() : (ytFiles.get(36) != null) ? ytFiles.get(36).getUrl() : "";
                    String downloadUrl = ytFiles.get(18).getUrl();
                    Log.d("SDLFKD", downloadUrl);

                }
            }
        }.extract(youtubeLink, true, true);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }
}
