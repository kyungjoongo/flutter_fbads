package jessica_labs.flutter_fbads.flutter_fbads;

import android.util.Log;
import android.view.View;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import com.facebook.ads.*;

/**
 * FlutterFbadsPlugin
 */
public class FlutterFbadsPlugin implements MethodCallHandler {
    Registrar registrar;

    public FlutterFbadsPlugin(Registrar registrar) {
        this.registrar = registrar;
    }


    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_fbads");
        channel.setMethodCallHandler(new FlutterFbadsPlugin(registrar));
    }

    private InterstitialAd interstitialAd;

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        if (call.method.equals("getPlatformVersion")) {
            result.success("Android " + android.os.Build.VERSION.RELEASE);
        } else {
            result.notImplemented();
        }


        if (call.method.equals("showInitAd")) {
            Log.d("sdlkfsdlfk", "showInitAd====>: ");

            String adId= call.argument("adId");

            if (interstitialAd != null) {
                interstitialAd.destroy();
            }
            interstitialAd = new InterstitialAd(registrar.context(), adId);
            interstitialAd.setAdListener(new InterstitialAdListener() {
                @Override
                public void onInterstitialDisplayed(Ad ad) {
                    // Interstitial ad displayed callback
                    Log.e("debug", "Interstitial ad displayed.");
                }

                @Override
                public void onInterstitialDismissed(Ad ad) {
                    // Interstitial dismissed callback
                    Log.e("debug", "Interstitial ad dismissed.");
                }

                @Override
                public void onError(Ad ad, AdError adError) {
                    // Ad error callback
                    Log.e("debug", "Interstitial ad failed to load: " + adError.getErrorMessage());
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    // Interstitial ad is loaded and ready to be displayed
                    Log.d("debug", "Interstitial ad is loaded and ready to be displayed!");
                    // Show the ad
                    interstitialAd.show();
                }

                @Override
                public void onAdClicked(Ad ad) {
                    // Ad clicked callback
                    Log.d("debug", "Interstitial ad clicked!");
                }

                @Override
                public void onLoggingImpression(Ad ad) {
                    // Ad impression logged callback
                    Log.d("debug", "Interstitial ad impression logged!");
                }
            });
            interstitialAd.loadAd();


            result.success("Android ");
        } else {
            result.notImplemented();
        }
    }



}
