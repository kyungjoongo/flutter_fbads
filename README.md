# flutter_fbads(Only Android)
Flutter Facebook Audience Network Interstitial Ad Plug in.
You can use  Facebook Audience Network Interstitial with this plugin in Flutter
Only on Android.
Banners, and native are not supported.

## pubspec.yml (install dependency)  
dependencies:  
&nbsp;&nbsp;&nbsp;&nbsp;flutter_fbads:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;git:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;url: https://gitlab.com/kyungjoongo/flutter_fbads.git  

## import package

import 'package:flutter_fbads/flutter_fbads.dart';

## usage

**//your facebook audience network Interstitial ad id.**  
String adId = '394084851148824_394085212345678';


**//show Interstitial ad .**  
FlutterFbads.showInitAd(adId);








