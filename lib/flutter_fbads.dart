import 'dart:async';

import 'package:flutter/services.dart';

class FlutterFbads {
    static const MethodChannel _channel =
    const MethodChannel('flutter_fbads');

    static Future<String> get platformVersion async {
        final String version = await _channel.invokeMethod('getPlatformVersion');
        return version;
    }

    static void showInitAd(String adId) async{
        var map={
            "adId" : adId

        };

        String result = await _channel.invokeMethod('showInitAd', map);
        print(result);
    }
}
